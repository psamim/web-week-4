import React from "react";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = { latitude: 0, longitude: 0 };
  }

  render() {
    window.navigator.geolocation.getCurrentPosition((position) => {
      console.log(position.coords.latitude);
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        name: "Samim",
      });
    });

    return (
      <div className="App">
        Hello from latitude: {this.state.latitude} <br />
        and longitude: {this.state.longitude}
      </div>
    );
  }
}

/*
function App() {
  return <div className="App">Hello</div>;
}
*/

export default App;
